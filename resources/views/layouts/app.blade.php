<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Ubikuo.GitHub</title>
        <meta name="description" content="@yield('meta_description', 'GITHUB')">
        <meta name="author" content="@yield('meta_author', 'Alejandro Nelis')">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">

    </head>
    <body>
        <script src="{{ url('js/jquery/jquery.min.js') }}"></script>
        <div id="app">
            <div class="container">
                @yield('content')
            </div><!-- container -->
        </div><!-- #app -->
    </body>
</html>
